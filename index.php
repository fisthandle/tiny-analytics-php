<?php

$time_start = microtime(true); 
$PASSWORD = 'plsshowstats';     // change your password here or create a config.php file: <?php $PASSWORD = '...';

session_set_cookie_params(30 * 24 * 3600, dirname($_SERVER['SCRIPT_NAME']));   // remember me
session_start();
if ($_GET['action'] === 'logout')
{
    $_SESSION['logged'] = 0;
    header('Location: .');   // reload page to prevent ?action=logout to stay
}
if ($_POST['pass'] === $PASSWORD || $_GET['pass']===$PASSWORD)
{
    $_SESSION['logged'] = 1;
    header('Location: .');   // reload page to prevent form resubmission popup when refreshing / this works even if no .htaccess RewriteRule 
}
if (!isset($_SESSION['logged']) || !$_SESSION['logged'] == 1)
{
    echo '<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>TinyAnalytics</title><link rel="icon" href="favicon.ico"></head><body><form action="." method="post"><input type="password" name="pass" value="" autofocus><input type="submit" value="Submit"></form></body></html>';
    exit();
}

/*
If you have lots of data (hundereds of thousands of views a day), maybe you want to calc the stats on a request only (or using cron).
if ($_GET['action'] === 'summarize'){
    include('summarize.php');
    header('Location: .');
}*/

//At the moment, we have little data, so lets just recalc it at every page refresh. Only todays data will be calculated, the rest is compiled already in .numbers files
include('summarize.php');
?>

<html>
<head>
<title>Stats</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<link rel="icon" href="favicon.ico">
<style type="text/css">
* { border: 0; outline: 0; padding: 0; margin: 0; font-family: sans-serif; }
html { position: relative; min-height: 100%; }
.chart { width: 51%; height: 250px; }
.referers { height: 190px; overflow-y: auto; width: 45%; float: right; font-size: 0.8em; white-space: nowrap; overflow-x: hidden; }
.referers a { text-decoration: none; }
.site { padding-bottom: 30px; margin-bottom: 30px; border-bottom: 1px solid #eee; }
.site h1 { margin-bottom: 0px; }
p.warning { margin-bottom: 10px; }
.code { background-color: #f4f4f4; font-family: monospace; padding: 3px; }
pre { margin-bottom: 10px; }
#content { padding: 10px 10px 60px 10px; }
#footer { margin-top: 80px; color: #333; font-size: 0.8em; position: absolute; bottom: 10px; left: 10px; }
#footer a { color: #333; }
@media (max-width: 600px) { .referers { display: none; } .chart { width: 100%; } }
</style>
<script src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
<div id="content">
<?php     
$sites = glob("./logs/*.numbers", GLOB_BRACE);

if (!is_writable(realpath(dirname(__FILE__)))) 
    echo '<p class="warning">&#8226; TinyAnalytics currently can\'t write data. Please give the write permissions to TinyAnalytics with:</p><pre class="code">chown -R ' . exec('whoami') . ' ' . realpath(dirname(__FILE__)) . '</pre>';
if (count($sites) == 0) 
    echo "<p class=\"warning\">&#8226; No analytics data yet. Add this tracking code in your website's main PHP file, and then visit it at least once.</p><pre class=\"code\">&lt;?php\ninclude '" . realpath(dirname(__FILE__)) . "/tracker.php';\nrecord_visit('mywebsite');\n?&gt;</pre>";

foreach ($sites as $site){
    $sitename = basename($site, '.numbers');

    //grab referers
    $referersfile = substr($site, 0, -8) . '.referers';
    $referers = '';
    $str = file_get_contents($referersfile);
    $urls = json_decode($str, true);
    arsort($urls);
    foreach ($urls as $url => $count){
        $url = preg_replace('/[\"<>]+/', '', $url); 
        $displayedurl = preg_replace('#^https?://#', '', $url);
        $referers .= '<p><a href="' . $url . '">' . $displayedurl . '</a> ('.$count.')</p>' . PHP_EOL;
    }   

    //get numerical data from file
    $file = $site;
    $str = file_get_contents($file);
    $data = json_decode($str, true);

    $views = '';
    $visitors = '';
    for ($i = 0; $i < 30; $i++) {
        $key = date("Y-m-d", time() - $i * 3600 * 24);
        $y = ((isset($data[$key])) ? $data[$key] : array('visitors' => 0, 'views' =>0));
        $views .= $y['views']. ",";
        $visitors .= $y['visitors'].",";
    }

    echo '<div class="site">' . PHP_EOL
        . '<div class="referers">' . PHP_EOL . $referers . '</div>' . PHP_EOL    
        . '<h1>' . $sitename . '</h1>' . PHP_EOL
        . '<div class="chart" visitors="' . substr($visitors,0,-1) . '" views="'. substr($views,0,-1).'"></div>' . PHP_EOL
        . '</div>' . PHP_EOL . PHP_EOL;
}
$end = floor((microtime(true) - $time_start) * 1000);
?>
</div>
<div id="footer">Tiny-Analytics v 1.1. Page took: <?=$end?> ms. <a href="?action=summarize">Reprocess data</a>. <a href="?action=logout">Logout</a>.</div>

<script type="text/javascript">
google.charts.load('current', { callback: function () { drawChart(); window.addEventListener('resize', drawChart, false); },  packages:['corechart'] });

function drawChart() {
    var formatDate = new google.visualization.DateFormat({ pattern: 'dd-MM' });
    var ticksAxisH;
    function createDataTable(visitors, views){
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('date', 'Day');
        dataTable.addColumn('number', 'U. visitors');
        dataTable.addColumn('number', 'Views');
        var today = new Date();
        ticksAxisH = [];
        for (var i = 0; i < visitors.length; i++) {
            var rowDate = new Date(today - i * 24 * 3600 * 1000);
            var xValue = { v: rowDate, f: formatDate.formatValue(rowDate) };
            var yValue = parseInt(visitors[i]);
            var y2Value = parseInt(views[i]);
            dataTable.addRow([xValue, yValue, y2Value]);
            if ((i % 7) === 0) { ticksAxisH.push(xValue); }      // add tick every 7 days
        }
        return dataTable;
    }

    var charts = document.getElementsByClassName("chart");
    for(var i = 0; i < charts.length; i++){
        var chart = new google.visualization.AreaChart(charts[i]);
        var visitors = charts[i].getAttribute('visitors').split(',');
        var views = charts[i].getAttribute('views').split(',');
        var dataTable = createDataTable(visitors, views);
        chart.draw(dataTable, {
            hAxis: { gridlines: { color: '#f5f5f5' }, ticks: ticksAxisH },
            legend: 'none',
            pointSize: 6,
            lineWidth: 3,
            colors: ['#058dc7'],
            areaOpacity: 0.1,
            series: {0: {targetAxisIndex:0},
                1:{targetAxisIndex:1}                  },
            colors: ["blue", "green"]
        });  
    }
}
</script>
</body>
</html>
