<?php 

//walk through every log
$sites = glob(realpath(dirname(__FILE__)) . "/logs/*.log", GLOB_BRACE);
foreach ($sites as $site){
	//get old stats
	$referersFile = substr($site, 0, -3) . 'referers';
	$numbersFile = substr($site, 0, -3) . 'numbers';
	//parse stats
	$refs = getJsonFromFile($referersFile);
	$numbers = getJsonFromFile($numbersFile);


	//setup vars
	$today = date("Y-m-d");
	$visitors = array();
	$views = array();

	//place to save todays log lines
	$todayLines = array();
	//place to add older lines (just in case, or for more, custom analytics)
	$oldFile = fopen($site.".old", "a");

	//get all current logs
	$logs = file($site);
	foreach ($logs as $line){

		$row = explode("\t", $line);
		$rowDate = date("Y-m-d", $row[0]);

		//user data
		$ip = $row[1];
        $ua = $row[3];         
        
        //ignore stats by crawlers (maybe extend list?)
        $exclude = array ('bot', 'crawl', 'slurp', 'spider', 'yandex', 'loader.io');
        foreach ($exclude as $excl){
        	if (stripos($ua, $excl) !== false){
        		continue 2;
        	}
        }

        //trim referer and save if not empty
        $ref = trim($row[4]);
        if (!empty($ref) && strlen($ref) > 5){
        	$refs[$ref] = isset($refs[$ref]) ? $refs[$ref] + 1: 1;
        }

        //add visitor IP address. later will filter only unique
        $visitors[$rowDate][]=$ip;
        if (!isset($views[$rowDate])){
        	$views[$rowDate] =0;
        }
        //save one view
        $views[$rowDate]++;

        //for today, save lines in current log
        if ($rowDate == $today){
        	$todayLines[] = $line;
        }else{
        	//for the past, add to backup log
        	fwrite($oldFile, $line);
        }
	}

	fclose($oldFile);

	//replace log with only todays data
	$fp = fopen($site, 'w');
	foreach ($todayLines as $line){
		fwrite($fp, $line);
	}
	fclose($fp);


	//walk throught all days in current log
	foreach (array_keys($views) as $day){
		//don't have this day, add it
		if (!isset($numbers[$day])){
			$numbers[$day] = array();
		}
		//save dataa
		$numbers[$day]['visitors'] = count(array_unique($visitors[$day]));
		$numbers[$day]['views'] = $views[$day];
	}

	//dump stats to file
	saveJsonToFile($referersFile, $refs);
	saveJsonToFile($numbersFile, $numbers);
}




##### FUNCTIONS

function getJsonFromFile($file){
	@$str = file_get_contents($file);
	if ($str === FALSE){
		return array();
	}
    @$urls = json_decode($str, true);
    if ($urls == NULL){
    	return array();
    }
    return $urls;
}

function saveJsonToFile($file, $data){
	$fp = fopen($file, 'w');
	fwrite($fp, json_encode($data));
	fclose($fp);
}