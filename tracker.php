<?php 
function record_visit($sitename){
    $sitename = strtolower($sitename);
    $ref = '';
    if (isset($_SERVER['HTTP_REFERER'])){
        $ref = $_SERVER['HTTP_REFERER'];    
        //don't save the same domain referers
        if (preg_match('/https?:\/\/(www\.)?'.$sitename.'/i', $ref)){
            $ref = '';
        }
    }
    $logfile = realpath(dirname(__FILE__)) . '/logs/' . $sitename . '.log';
    $txt = time() . "\t" . $_SERVER['REMOTE_ADDR'] . "\t" . $_SERVER['REQUEST_URI'] . "\t" . $_SERVER['HTTP_USER_AGENT'] . "\t" . $ref . PHP_EOL;
    file_put_contents($logfile, $txt, FILE_APPEND);
}
?>
