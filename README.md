# TinyAnalyticsPHP

![](./screenshot.png)

**TinyAnalyticsPHP** is a very lightweight web analytics tool based on [TinyAnalytics](https://github.com/josephernest/TinyAnalytics) by Joseph Ernest. **200** lines of PHP (with comments). What it does:
* Display only the **most necessary stats**: unique visitors, page views, referers, on a single page.
* Don't waste time and resources with request based analytics
* Do without constantly growing database of useless metrics
* Fast, light, unnoticable load

TinyAnalyticsPHP grabs only time, Request URI, IP, UserAgent and Referer (if any). Need any more data? Better get a different analytics.
Works well in a multi domain hosting. Probably less useful with one domain per machine/user setup.

## Install

There are three easy steps:

1) Unzip this package in a directory, e.g. `/var/www/TinyAnalytics/`.

2) Add the following tracking code to your websites at then end of `.php` files, e.g. `/var/www/mywebsite/index.php`:

    `<?php 
    include '/var/www/TinyAnalytics/tracker.php';
    record_visit('domain.eu');
    ?>`

3) Modify your password in line 5 of `index.php`. Default password is `plsshowstats`.    

It's done! Visit at least one of your tracked websites, and open `TinyAnalytics/index.php` in your browser. 
You can login as well using a GET url param: `TinyAnalytics/index.php?pass=plsshowstats`. 
Security wasn't the primary concern here.

## My improvements
* No python code, everything is PHP
* Added page views metric
* Old logs are stored in another file `domain.eu.log.old` in case you want to do more analysis on them. Consider not storing `old` files if you value your server space
* Referers are counted and sorted by count 
* Referers list exclude the analyzed domain IF you use domain when adding tracking code, like: `record_visit('domain.com')`

## About
Original idea: Joseph Ernest

License: MIT
